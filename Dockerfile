FROM alpine
WORKDIR /build
RUN apk add git make gcc libc-dev musl-dev glib-static gettext eudev-dev linux-headers automake autoconf cmake meson ninja clang go-md2man

RUN git clone https://github.com/libfuse/libfuse \
    && cd libfuse \
    && git checkout fuse-3.14.0 \
    && mkdir build \
    && cd build \
    && LDFLAGS="-lpthread -s -w -static" meson --prefix /usr -D default_library=static .. \
    && ninja \
    && ninja install

RUN git clone https://github.com/containers/fuse-overlayfs \
  && cd fuse-overlayfs \
  && git checkout v1.10
RUN cd fuse-overlayfs && \
    ./autogen.sh && \
    LIBS="-ldl" LDFLAGS="-s -w -static" ./configure --prefix /usr && \
    make clean && \
    make && \
    make install

USER user
ENV USER user
ENV HOME /home/user
ENV XDG_RUNTIME_DIR=/run/user/1000
WORKDIR /home/user